# GoodProject
# Практическая работа №5
**Программа** выводит чаще повторяющиеся триграммы
1. Входные значения: строка заглавный английских буков
2. Функция check проверяет коректность ввода
3. Функция s_ t _r Выводит чаще повторяющиеся триграммы
**Пример** работы алгоритма
* "AAAABBBB"
* output = ['AAA' , 'BBB']
код в языке python выгледит так

```
def check(inputstr):
    global error
    error = True
    for i in inputstr:
        if ord(i) < 65 or ord(i) > 90:
            error = False
            return False
    if error != False:
        return inputstr


def s_t_r(inputstr):
    if inputstr != 'Ошибка, введите заглавные буквы от A до Z':
        output = []
        trigram = {}
        for i in range(len(inputstr) - 2):
            s = ''.join(sorted(inputstr[i:i + 3]))
            if trigram.get(s) is not None:
                trigram[s] += 1
            else:
                trigram[s] = 0
                    
        maxtr = max(trigram.values())
        for i in trigram:
            if trigram[i] == maxtr:
                output.append(i)
        return output
    else:
        return 'Ошибка, введите заглавные буквы от A до Z'
        

def main():
    a = input()
    a = check(a)
    if a != False:
        print(s_t_r(a))
    else:
        print('Ошибка, введите заглавные буквы от A до Z')
    #print(check(s_t_r(input())))



if __name__ == "__main__":
    main()
```


![](https://sun9-51.userapi.com/c854020/v854020468/1660fe/-6yQzUfmX-g.jpg)

[ридми файлик](https://coddism.com/zametki/razmetka_readmemd_v_github)
