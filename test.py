import pytest
from lab import s_t_r
from lab import check


def test_check_1():
    assert check('AAAABBBB') == ('AAAABBBB')
def test_check_2():
    assert check('aaassassa') == ('Ошибка, введите заглавные буквы от A до Z')
def test_check_3():
    assert check('1221312') == ('Ошибка, введите заглавные буквы от A до Z')
def test_check_4():
    assert check('!?') == ('Ошибка, введите заглавные буквы от A до Z')
def test_check_5():
    assert check('AA1212!112dasds') == ('Ошибка, введите заглавные буквы от A до Z')

def test_STR_1():
    assert s_t_r('AAAABBBB') == (['AAA', 'BBB'])
def test_STR_2():
    assert s_t_r('QWERTY') == (['EQW', 'ERW', 'ERT', 'RTY'])
def test_STR_3():
    assert s_t_r('QQQRQQQNQQQBQQQ') == (['QQQ'])
def test_STR_4():
    assert s_t_r('FFGGHH') == (['FFG', 'FGG', 'GGH', 'GHH'])
def test_STR_5():
    assert s_t_r('MMMMMMMMMMMMMMMM') == (['MMM'])

    
