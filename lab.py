def check(inputstr):
    global error
    error = True
    for i in inputstr:
        if ord(i) < 65 or ord(i) > 90:
            return 'Ошибка, введите заглавные буквы от A до Z'
            error = False
            break
    if error != False:
        return inputstr


def s_t_r(inputstr):
    if error == True:
        output = []
        trigram = {}
        for i in range(len(inputstr) - 2):
            s = ''.join(sorted(inputstr[i:i + 3]))
            if trigram.get(s) is not None:
                trigram[s] += 1
            else:
                trigram[s] = 0
                    
        maxtr = max(trigram.values())
        for i in trigram:
            if trigram[i] == maxtr:
                output.append(i)
        return output


def main():
    print(s_t_r(check(input())))


if __name__ == "__main__":
    main()
